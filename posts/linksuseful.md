---
layout: post
title: "useful links (general)"
categories: "IntroPack"
date = 2017-11-24T14:17:25+01:00
---

# PhD useful links (general)
[Handbook for doctoral studies](https://www.chalmers.se/insidan/EN/education-research/doctoral-student/handbook-for-doctoral8636)
Procedure for licentiate Thesis, followup meetings and needed forms

[Doctoralportal](https://student.portal.chalmers.se/doctoralportal/Pages/Doctoralportal.aspx)

[Insidan](https://www.chalmers.se/insidan/EN/)

[Welcome leaflet](http://wiki.portal.chalmers.se/cse/uploads/PhDStudents/welcome_leaflet.pdf) with a bunch of useful organizational information (also for non-PhDs)

[Licentiate degree](http://www.chalmers.se/insidan/sites/cse/doctoral-ed/phd-student/licentiate-degree)

[Follow-up meetings](http://www.chalmers.se/insidan/sites/cse/doctoral-ed/phd-student/follow-up-meetings)
