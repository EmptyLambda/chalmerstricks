---
layout: post
title: "Student_union"
categories: "IntroPack"
date = 2017-11-24T14:17:25+01:00
---


## Student union
As a Chalmers PhD student you have the right (and the obligation) to be part of the Student Union. 
Sign up to the [Student Union](http://medlem.chs.chalmers.se), this automatically gives you 3 months in the SGS queue 
and gives you access to all student discounts [Mecent](https://mecenat.com/se/). 
The fee is 250 SEK fee every semester. 

** Student union card is REQUIRED for taking an exam **
**NOTE:** If you are a Chalmers PhD student, in order to graduate you need to prove you have payed the fee to the Student Union for all the semesters you have work at Chalmers. 
