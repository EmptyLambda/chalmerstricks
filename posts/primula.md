---
layout: post
title: "primula"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---


## Report vacations, leave, expenses

You report vacation application, leave, expenses compensations in  
[Primula](https://personal.portal.chalmers.se/chalmers/)

login: CID

password: CID password

Here you can also check how many vacation days you have left (or accumulated), your employment statements and records. 

Do not forget to keep you *Persona Data* page up-to-date (in particular when you move)
