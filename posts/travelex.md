---
layout: post
title: "Travelling as a Chalmers' Employee"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---

## Travel Approval

**Before you go:** Find out cost estimate, check with your supervisor if your project can cover the cost. 
Fill in the new [e-form](https://docs.google.com/forms/d/e/1FAIpQLSdZDN5LPn2lget_3JQWUJImnJMx5TM7WjClmOJmVl2TIcMoyQ/viewform) 
(the form will be sent to your line manager for signature and to your division secretary for reference).  
Book the travel using the Chalmers (or GU) travel agent ([egencia](https://idp.chalmers.se/adfs/ls/?SAMLRequest=hZHNbsIwEIRfxfI9cf5KwCJBtKgqElURhB56qRzHBkuJnWYd6OPXAiLBhR5XnvHMfjud%2FTY1OooOlNEZDv0AI6G5qZTeZ3hXvHpjPMunwJo6aum8twe9ET%2B9AIucUQO9vGS47zQ1DBRQzRoB1HK6nb%2BvaOQHtO2MNdzUGM0BRGdd1IvR0Dei24ruqLjYbVYZPljbAiXkdDr5Yu9aKOZz0xDmUskxJMw5JeN28GK0cD2UZvbcfbCrqvX5gdVOAT4IwioJpAaC0XKR4e%2BEJ6kUcTouR2WVBDJOJzKNZcWCciTLUDoZQC%2BWGizTNsNREKZeGHpRUoQxfZrQJPEnUfqF0fq61rPSF1yPGJQXEdC3olh7649tgdHngN0J8BUyPad3t3Qff8wGpDj%2FB%2BCU3Cbk1%2FH%2Bqvkf&RelayState=Y2FsbEJhY2tVUkw6PTphSFIwY0hNNkx5OTNkM2N1WldkbGJtTnBZUzV6WlM5aGNIQS9jMlZ5ZG1salpUMWxlSFJsY201aGJDWndZV2RsUFV4dloybHVKbTF2WkdVOVptOXliU1p0WVhKclpYUTlVMFU9X3xfY29tcGFueUlkOj06QUg0aDZEREhUUjRTSzhJRkdPZVkwUV98X3NvdXJjZTo9OlJWVT1ffF92YW5pdHlVcmw6PTpvRTVqcWE3djJCUTBKLTJZOXFNTTk5VVFTemJJdEtyTy15ZVNmQ2dQUVN3)). 

**Travel:** Enjoy it, pay with your Chalmers MasterCard or in other ways, just remember to keep receipts for any non-food expense (e.g. taxi, train tickets..)

**After:** To be refunded, fill in a travel-expenses claim in [primula](https://personal.portal.chalmers.se/chalmers/) 
within *one month*. Send all your receipts to LRS using one of the large yellow folders available in the post-room on the 4th floor, 
cross away the previous addressee and write LRS, close the folder and leave it on the 'outgoing internal mail' pile.

**Mortal deadline:** after *three months* the claim is no longer payable. 


# How to fill in the report:

- For each meal _M_ on each day there are three options:
    1. Checking box **with meal benefits** (**B**): choose this option if meal _M_ is included as part of another fee (for example, a lunch that is included in a conference registration fee) **and** _M_ is not a breakfast.
    2. Checking box **without meal benefits** (**N**): choose this optin if meal _M_ is offered by a third party (for example, if you are a guest and your host pays for _M_) **or** _M_ is a breakfast (which is usually included in hotel fee)
    3. Not checking either box (**E**): choose this option if you pay for meal _M_ yourself (for example, you go out to a restaurant and pay your share of the bill out of your own pocket) (Don't clicking on anything)

The consequences of these options are:

1. **B** (with meal benefits) implies that you will not get an allowance for meal _M_, but Chalmers paid indirectly (for example through the registration fee) and will pay taxes accordingly.
2. **N** (without meal benefits) implies that you will not get an allowance for meal _M_, and Chalmers did not pay either.
3. **E** (not clicking on anything) implies that you will get an allowance for meal _M_. Note, however, that the allowance is a fixed amount: you do not get reimbursed what you paid exactly (and hence you do not need to keep receipts of meal expenses).



# Book a trip via egencia online

[Register](http://go.egencia.com/R00y00fT0X003000FrYu6S0)
The first attempt for registration usually fails :), don't get disappointed, try again

[Booking](http://chalmers.egencia.se/app?service=external&page=Login&mode=form&market=SE)
Email address to ask questions or order via email: Team3.got@viaegencia.com costumer service is better when you email them. 