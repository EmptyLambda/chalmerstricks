---
layout: post
title: "Library"
categories: "IntroPack"
date = 2017-11-24T14:17:25+01:00
---

## Access material when you off-campus

## Add your publications to CLP

## Books
You can request and access material such as books and articles you need for you research at the [Library website](http://www.lib.chalmers.se/en/search/purchase-suggestions-and-interlibrary-loans/) 
You can request/recommand a (paper/online) book for library. (the book doesn't necessary need to be relavant to your research area).


**Lending times:**

Books : 1-3 weeks 

E-book, articles: 1-3 days
