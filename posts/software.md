---
layout: post
title: "software"
categories: "IntroPack"
date = 2017-11-24T14:17:25+01:00
---

## Free commercial softwares
Worth looking (OS and utitily software(antivirus) and domain specific softwares like MATLAB and ...)
Windows share:

- smb://syros.ce.chalmers.se
- \\\\syros.ce.chalmers.se

- Domain: NET
- username: CID

[syros directory tree](syrosdirtreed3.txt) output of "tree -L 3 -d" on smb://syros.ce.chalmers.se/program/

## Microsoft tools
Different version of Windows (even ancient DOS), SQL server and visual studio and more. You need to specify your university (Chalmers).

[Microsoft tools](https://onthehub.com/microsoftimagine) Almost everything except MS-Office which is accessable from SYROS server.


##Box account
As a Chalmers employee you get access to a Box account, which is like Dropbox, but larger (32GB?). 
To start using it:

- [box](https://www.box.com/en-gb/home): 
 - select 'sign-in' (top right)
 - type in CID@chalmers.se
 - click on 'Sign in with SSP' 
 - type in 'Chalmers', click ad log in with your CID and CID password