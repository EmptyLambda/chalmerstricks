---
layout: post
title: "email"
categories: "IntroPack"
date = 2017-11-24T14:17:25+01:00
---

##Setting up your Chalmers email account


# IMAP
- imap.chalmers.se - port: 993
- username : CID
- Auth method: CID password
- Connection: SSL/TLS
// Note : Thunderbird test doesn't work correctly.

# SMTP
- smtp.chalmers.se : 587
- username : CID
- Auth method: NTLM (TLS also works)
- Connection: STARTTLS (if required)

# iOS
[Detail setting from IT portal](https://it.portal.chalmers.se/itportal/MobilSurfPlattaApple/Epost)
- Select Exchange account
- On the first page, use 'CID@chalmers.se'
- Domain: NET
- Server: webmail.chalmers.se
// Note: if you found some information page from Chalmers website saying the server "m.outlook.com", that is intended for Chalmers student

# Thunderbird/Outlook/Exchange integration
Tools -> addon -> "Lightning"

[Exchange calendar](https://github.com/Ericsson/exchangecalendar/releases) Get direct access to Chalmers ldap contacts in thunderbird using this extension

# Free commercial services
- [Sharelatex](https://sharelatex.com/)
- [Bitbucket](http://bitbucket.com)
- [Github](http://github.com)
- [Box](http://box.com)
- [Prezi](http://prezi.com)
