---
layout: post
title: "Delete Vacation"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---



## Vacation Deletion (for Chalmers employees)

Sometimes you book vacations ahead of time and you have to then re-arrange the dates. 
This is not possible through primula website. You need to call LRS at the (internal Chalmers number) 26-32 between 9.00am and 11.00am on working days, and inform your supervisor/line manager about the change.