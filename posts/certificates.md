---
layout: post
title: "Certificates"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---

## List of passed courses and certificate

Holding a list of passed courses is useful when applying for scholarship, and to submit your Licentiate / PhD defence request.

[Student portal](https://student.portal.chalmers.se/en/chalmersstudies/Pages/services.aspx)
